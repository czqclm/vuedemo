// 引入了axios
import axios from 'axios'
import Vue from 'vue'
import router from '@/router'
// 需要额外引入 qs ，qs 将对象序列化成URL的形式，以&进行拼接 或者将拼接好的 URL 拆分成 对象
import qs from 'qs'
// 这里映入的是 饿了吗的组件中的MessageBox 用来做弹窗
import {MessageBox} from 'element-ui'
import merge from 'lodash/merge'

// 通过对 axios.create 创建的一个实例，对实例进行一些配置，
// 便可以得到一个专门与后端进行通讯的 ajax 函数
const http = axios.create({
  timeout: 1000 * 30,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json; charset=utf-8'
  }
})

/**
 * 请求拦截 (请求的时候)
 * 将所有发送给后台的请求，都带上一个 token 的 值
 * token的值来于 cookie “Vue.cookie.get('token')” 就是用来取cookie中我们存储的值
 * cookie 是存储在用户浏览器中的文本文件
 * token相当于一个令牌，我们有这个令牌后台才知道我们是谁，是否有权限
 */
http.interceptors.request.use(config => {
  config.headers['token'] = Vue.cookie.get('token') // 请求头带上token
  return config
}, error => {
  return Promise.reject(error)
})

/**
 * 响应拦截（返回数据的时候）
 * 
 * 这里的token需要参考那个项目中的token是怎么命名的
 */
http.interceptors.response.use(response => {
  if (response.data && response.data.code === 401) { // 401, token失效
    // 如果发现登陆不了了 就是用户登陆过期了 那么我们就把我们cookie的token删除
    // 调用登陆的时候，登陆成功，后台会给我们返回一个 token 我们把它存到cookie中就可以了
    Vue.cookie.delete('token')
    MessageBox.confirm('相关操作需要登录后才能继续，是否登录？', '提示', {
      confirmButtonText: '确定',
      type: 'warning'
    }).then(() => {
      router.push({ name: 'userlogin' }, () => {
        location.reload() // 刷新页面, 清空整站临时存储数据
      })
    })
  }
  return response
}, error => {
  return Promise.reject(error)
})

/**
 * 请求地址处理
 * @param {*} actionName action方法名称
 */
http.adornUrl = (actionName) => {
  // 接口前缀统一使用[/proxyApi/]前缀做代理拦截!
  // 这里需要看vue.config.js的配置
  return '/proxyApi/' + actionName
}

/**
 * get请求参数处理
 * @param {*} params 参数对象
 * @param {*} openDefultParams 是否开启默认参数?
 */
http.adornParams = (params = {}, openDefultParams = true) => {
  var defaults = {
    't': new Date().getTime()
  }
  return openDefultParams ? merge(defaults, params) : params
}

/**
 * post请求数据处理
 * @param {*} data 数据对象
 * @param {*} openDefultdata 是否开启默认数据?
 * @param {*} contentType 数据格式
 *  json: 'application/json; charset=utf-8'
 *  form: 'application/x-www-form-urlencoded; charset=utf-8'
 */
http.adornData = (data = {}, openDefultdata = true, contentType = 'json') => {
  var defaults = {
    't': new Date().getTime()
  }
  data = openDefultdata ? merge(defaults, data) : data
  return contentType === 'json' ? JSON.stringify(data) : qs.stringify(data)
}

export default http
