export default {
  namespaced: true,
  state: {
    id: 0,
    name: '',
    nickName: '',
    loginState: 0,
    mobile: ''
  },
  mutations: {
    updateId (state, id) {
      state.id = id
    },
    updateName (state, name) {
      state.name = name
    },
    updateNickName (state, nickName) {
      state.nickName = nickName
    },
    updateLoginState (state, loginState) {
      state.loginState = loginState
    },
    updateMobile (state, mobile) {
      state.mobile = mobile
    }
  }
}
