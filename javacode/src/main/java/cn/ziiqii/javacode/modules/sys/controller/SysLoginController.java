package cn.ziiqii.javacode.modules.sys.controller;

import cn.ziiqii.javacode.common.utils.R;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("sys")
public class SysLoginController {

    @RequestMapping("/hint")
    public R hint() {
        return R.ok().put("msg","哈哈哈，你启动了你的vue项目");
    }
}
